//
//  ZFAppDelegate.h
//  IJKMediaFramework
//
//  Created by 任子丰 on 06/13/2018.
//  Copyright (c) 2018 任子丰. All rights reserved.
//

@import UIKit;

@interface ZFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
