//
//  main.m
//  IJKMediaFramework
//
//  Created by 任子丰 on 06/13/2018.
//  Copyright (c) 2018 任子丰. All rights reserved.
//

@import UIKit;
#import "ZFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZFAppDelegate class]));
    }
}
